import React from 'react';
class Product extends React.Component {
  render() {
    return (
      <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div className="thumbnail">
          <img
            src={this.props.img}
            alt={this.props.name}
            />
          <div className="caption">
            <h3>{this.props.name}</h3>
            <p>
             {Number(this.props.price).toLocaleString()} VNĐ
            </p>
            <p>
              <button className="btn btn-primary">Mua Ngay</button>
            </p>
          </div>
        </div>
      </div>
    )
  }
}
export default Product;
