import React from 'react';
import Product from './component/Product'
import Data from './data/Data'
class Props extends React.Component {
  
  render() {
    let elements = Data.map((data)=>{
    return(
      <Product
          name={data.name}
          price={data.price}
          img={data.img}
          />
    )
  })
    return (
      console.log(Data),
      <div className="container">
        {elements}
      </div>

    )
  }
}
export default Props;
