import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Props from './Props';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Props/>, document.getElementById('root'));
serviceWorker.unregister();
